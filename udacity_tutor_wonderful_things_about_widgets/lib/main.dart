import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text('Hello widgets'),
      ),
      body: HelloContainer(),
    ),
  ));
}

class HelloContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          color: Colors.purple,
          width: 300.0,
          height: 400.0,
          margin: EdgeInsets.all(16.0),
          child: HelloColumn()),
    );
  }
}

class HelloColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text('Hello!'),
        Text('Hello!'),
        Text('Hello!'),
        Text('Hello!'),
      ],
    );
  }
}
