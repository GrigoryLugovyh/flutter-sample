import 'package:flutter/material.dart';

class CategoryModel {

  final String text;
  final IconData icon;

  CategoryModel(this.text, this.icon);
}

class CategoryWidget extends StatelessWidget {

  final CategoryModel model;

  CategoryWidget(this.model) {
    assert(model.text != null);
    assert(model.icon != null);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: 100.0,
        child: InkWell(
          onTap: () {
            print('I was tapped on ${model.text}');
          },
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Center(
                    child: Icon(
                        model.icon,
                        size: 60.0),
                  ),
                ),
                Center(
                  child: Text(
                    model.text,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
