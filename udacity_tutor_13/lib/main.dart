import 'package:flutter/material.dart';
import 'category_route.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CategoryRoute(),
    );
  }
}
