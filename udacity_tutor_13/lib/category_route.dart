import 'package:flutter/material.dart';
import 'category.dart';

class CategoryRoute extends StatelessWidget {
  Widget _listOfCategories(List<Widget> categories) => Container(
        child: ListView.builder(
            padding: EdgeInsets.all(8.0),
            itemCount: categories.length,
            itemBuilder: (BuildContext context, int index) {
              return categories[index];
            }),
      );

  final _backgroundColor = Colors.greenAccent;

  final _categoryModels = <CategoryModel>[
    CategoryModel('Add', Icons.add),
    CategoryModel('Build', Icons.build),
    CategoryModel('Airplane active', Icons.airplanemode_active),
    CategoryModel('Accessibility', Icons.accessibility),
    CategoryModel('Adb', Icons.adb),
    CategoryModel('Print', Icons.print),
    CategoryModel('Local ariport', Icons.local_airport),
    CategoryModel('Ac unit', Icons.ac_unit),
    CategoryModel('Access alarm', Icons.access_alarm),
    CategoryModel('Balance', Icons.account_balance),
    CategoryModel('Wallet', Icons.account_balance_wallet),
    CategoryModel('Account box', Icons.account_box),
    CategoryModel('Circle', Icons.account_circle),
    CategoryModel('A photos', Icons.add_a_photo),
    CategoryModel('Add alert', Icons.add_alert),
    CategoryModel('Location', Icons.add_location),
    CategoryModel('To photos', Icons.add_to_photos),
    CategoryModel('Airplay', Icons.airplay),
    CategoryModel('All inclusive', Icons.all_inclusive),
  ];

  @override
  Widget build(BuildContext context) {
    final categoryWidgets = <Widget>[];

    _categoryModels
        .forEach((model) => categoryWidgets.add(CategoryWidget(model)));

    final bar = AppBar(
      elevation: 0.0,
      title: Text(
        'Unit Converter',
        style: TextStyle(
          color: Colors.black,
          fontSize: 30.0,
        ),
      ),
      centerTitle: true,
      backgroundColor: _backgroundColor,
    );

    final body = Container(
        color: _backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: _listOfCategories(categoryWidgets));

    return Scaffold(
      appBar: bar,
      body: body,
    );
  }
}
